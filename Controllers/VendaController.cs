using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Consumes("application/json")]
    [Produces("application/json")]


    public class VendaController : ControllerBase
    {
            private readonly PaymentContext _context;
        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("post")]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = venda.VendaId }, venda);
        }

        [HttpGet("get-all")]
        public IActionResult GetAll()
        {
            var venda = _context.Vendas;
            return Ok(venda);
        }

        [HttpGet("get-by/{id}")]
        public IActionResult GetById(int id)
        {
            var venda = _context.Vendas.Find(id);
            {
                if (venda == null)
                {
                    return NotFound();
                }
            }
            return Ok(venda);
        }

        [HttpPut("update/{id}")]
        public IActionResult Update(int id, StatusEnum status)
        {
           var venda = _context.Vendas.Find(id);

            if (venda == null){
                return NotFound();
            }

            if (venda.Status == StatusEnum.AguardandoPagamento
                && status == StatusEnum.PagamentoAprovado
                || venda.Status == StatusEnum.AguardandoPagamento
                && status == StatusEnum.Cancelada)
            {
                venda.Status = status;
            }
            else if (venda.Status == StatusEnum.AguardandoPagamento
                    && status == StatusEnum.EnviadoParaTransportadora
                    || venda.Status == StatusEnum.PagamentoAprovado
                    && status == StatusEnum.Cancelada)
            {
                venda.Status = status;
            }
            else if (venda.Status == StatusEnum.EnviadoParaTransportadora 
                    && status == StatusEnum.Entregue)
            {
                venda.Status = status;
            }
            else
            {
                return BadRequest($"{venda.Status} não pode ser alterado {status}");
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok("A venda foi atualizada");
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Deletar(int id)
        {
            var dbtask = _context.Vendas.Find(id);

            if (dbtask == null)
                return NotFound();
                
            _context.Vendas.Remove(dbtask);
            _context.SaveChanges();
            
            return NoContent();
        }
        
    }
}