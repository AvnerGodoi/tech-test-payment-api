using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [Consumes("application/json")]
    [Produces("application/json")]

    public class ProdutoController : ControllerBase
    {

     private readonly PaymentContext _context;

         public ProdutoController( PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("post")]
        public IActionResult Create(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { id = produto.ProdutoId }, produto);
        }

        [HttpGet("get-all")]
        public IActionResult GetAll()
        {
            var produto = _context.Produtos;
            return Ok(produto);
        }

        [HttpGet("get-by/{id}")]
        public IActionResult GetById(int id)
        {
            var produto = _context.Produtos.Find(id);
            {
                if (produto == null)
                {
                    return NotFound();
                }
            }
            return Ok(produto);
        }

        [HttpPut("update/{id}")]
        public IActionResult Update(int id, Produto produto)
        {
            var dbtask = _context.Produtos.Find(id);

            if (dbtask == null)
                return NotFound();

            dbtask.Nome = produto.Nome;
            dbtask.Descricao = produto.Descricao;
            dbtask.Valor = produto.Valor;
            _context.Produtos.Update(dbtask);
            _context.SaveChanges();

            return Ok(dbtask);
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            var dbtask = _context.Produtos.Find(id);

            if (dbtask == null)
                return NotFound();
                
            _context.Produtos.Remove(dbtask);
            _context.SaveChanges();
            
            return NoContent();
        }

    }
}