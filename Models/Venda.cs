using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int VendaId { get; set; }
        public int ProdutoId { get; set; }

        public string Produto{ get; set; }
        public string VendedorId { get; set; }
        public string NomeVendedor{ get; set; }

        public decimal ValorVenda { get; set; }

        public StatusEnum Status { get; set; }




    }
}