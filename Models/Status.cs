

namespace tech_test_payment_api.Models
{
    public enum StatusEnum

{
    AguardandoPagamento,
    PagamentoAprovado,
    EnviadoParaTransportadora,
    Cancelada,
    Entregue
}
}